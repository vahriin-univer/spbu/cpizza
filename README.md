# CPizza

Clone this repo.

Run `deploy.sh` for deploy Docker containers. Now you in iroha container.

Run

```bash
irohad --config config.docker --genesis_block genesis.block --keypair_name node0 --overwrite_ledger
```

in iroha container. The irohad daemon will start and initialize first transaction.

Open new terminal and go to repo directory.

Run `python prepare.py` if you want to get some assets automatically.

Run `python main.py` in terminal.

Go to http://127.0.0.1:5000 to use CPizza. Supported names: client, franchise, stock, pizzeria.

Go to http://127.0.0.1:5000/admin to admin page.

![Proof of work](https://imgur.com/download/3nKqyT7)