from flask import Flask, render_template, request
from iroha_lib import User, Admin

app = Flask(__name__)


def get_account_assets(account):
    user = User(account)
    return user.get_assets()


@app.route("/", methods=["GET", "POST"])
def admin_page():
    if request.method == "POST":
        name = request.form["who"]
        if name == "client":
            return render_template("user.html", **get_account_assets("client"))
        elif name == "pizzeria":
            return render_template("pizzeria.html", **get_account_assets("pizzeria"))
        elif name == "franchise":
            return render_template("franchise.html", **get_account_assets("franchise"))
        elif name == "stock":
            return render_template("stock.html", **get_account_assets("stock"))
        else:
            return "Nick not found"
    else:
        return render_template("name.html")


@app.route("/send/<sender>", methods=["POST"])
def send(sender=None):
    user = User(sender)
    recipient = request.form["recipient"]
    amount = request.form["amount"]
    if sender == "stock" or sender == "admin":
        asset = request.form["asset"]
    else:
        asset = "coin"
    return user.transfer_asset(recipient, asset, amount)


@app.route("/buy", methods=["POST"])
def buy():
    client = User("client")
    pizzeria = User("pizzeria")
    return f"{client.transfer_asset(pizzeria.nick, 'coin', 5)} || {pizzeria.transfer_asset(client.nick, 'pizza', 1)}"


@app.route("/makepizza", methods=["POST"])
def makepizza():
    pizzeria = User("pizzeria")
    return pizzeria.exchange_asset("product", 2, "pizza", 1)


@app.route("/admin", methods=["GET", "POST"])
def admin():
    admin = Admin("admin")
    if request.method == "POST":
        asset = request.form["asset"]
        amount = request.form["amount"]
        return admin.add_coin(asset, amount)
    else:
        return render_template("admin.html", name="admin", **admin.get_assets())


if __name__ == "__main__":
    app.run(debug=True)
